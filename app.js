var express = require("express")
var app = express()

app.get("/version", function (req, res) {
	res.send("0.1");
});

var server = app.listen(4321, function () {
	console.log('Listening on 4321');
});
