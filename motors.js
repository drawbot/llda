var gpio = require('rpi-gpio');
var sleep = require('sleep');

// var StepPins = [17,18,27,22]; // BCM
var StepPins = [11,12,13,15]; // Board V1 and V2



var StepCounter = 0;
var WaitTime = 1;

// Define simple sequence
var SeqLength1 = 4;
var Seq1 = [
    [1,0,0,0],
    [0,1,0,0],
    [0,0,1,0],
    [0,0,0,1]
];

// Define advanced sequence
// as shown in manufacturers datasheet
var SeqLength2 = 8
var Seq2 = [
    [1,0,0,0],
    [1,1,0,0],
    [0,1,0,0],
    [0,1,1,0],
    [0,0,1,0],
    [0,0,1,1],
    [0,0,0,1],
    [1,0,0,1]
];

var Seq = Seq1;
var SeqLength = SeqLength1;




function GPIO_write(pinNumber, pinValue)
{
    console.log("GPIO_write (" + pinNumber + "," + pinValue + ")");

    gpio.write(pinNumber, pinValue, function(err) {
        if (err) throw err;
        console.log('Written to pin');
    });
}

function GPIO_SetOutput_1_ON()
{
    console.log("test 1");
    GPIO_write(StepPins[0], true);
    console.log("test 2");
}
function GPIO_SetOutput_1_OFF(){
    GPIO_write(StepPins[0], false);
}
function GPIO_SetOutput_2_ON(){
    GPIO_write(StepPins[1], true);
}
function GPIO_SetOutput_2_OFF(){
    GPIO_write(StepPins[1], false);
}
function GPIO_SetOutput_3_ON(){
    GPIO_write(StepPins[2], true);
}
function GPIO_SetOutput_3_OFF(){
    GPIO_write(StepPins[2], false);
}
function GPIO_SetOutput_4_ON(){
    GPIO_write(StepPins[3], true);
}
function GPIO_SetOutput_4_OFF()
{
    GPIO_write(StepPins[3], false);
}


function GPIO_SetOutput(motorPinNumber, motorPinValue)
{
    console.log("[" + motorPinNumber + "] " +  StepPins[motorPinNumber] + " : " + motorPinValue);

    switch(motorPinNumber)
    {
        case 0:
            if(motorPinValue)
                gpio.setup(StepPins[0], gpio.DIR_OUT, GPIO_SetOutput_1_ON);
            else
                gpio.setup(StepPins[0], gpio.DIR_OUT, GPIO_SetOutput_1_OFF);
            break;
        case 1:
            if(motorPinValue)
                gpio.setup(StepPins[1], gpio.DIR_OUT, GPIO_SetOutput_2_ON);
            else
                gpio.setup(StepPins[1], gpio.DIR_OUT, GPIO_SetOutput_2_OFF);
            break;
        case 2:
            if(motorPinValue)
                gpio.setup(StepPins[2], gpio.DIR_OUT, GPIO_SetOutput_3_ON);
            else
                gpio.setup(StepPins[2], gpio.DIR_OUT, GPIO_SetOutput_3_OFF);
            break;
        case 3:
            if(motorPinValue)
                gpio.setup(StepPins[3], gpio.DIR_OUT, GPIO_SetOutput_4_ON);
            else
                gpio.setup(StepPins[3], gpio.DIR_OUT, GPIO_SetOutput_4_OFF);
            break;
        default:
             console.log("Uncatched motorPinNumber : " + motorPinNumber);
    }
}


exports.reset = function reset()
{
    console.log("-- RESET PINS -- ");

    gpio.setup(StepPins[0], gpio.DIR_OUT, GPIO_SetOutput_1_OFF);
    gpio.setup(StepPins[1], gpio.DIR_OUT, GPIO_SetOutput_2_OFF);
    gpio.setup(StepPins[2], gpio.DIR_OUT, GPIO_SetOutput_3_OFF);
    gpio.setup(StepPins[3], gpio.DIR_OUT, GPIO_SetOutput_4_OFF);

    console.log(" --- Pause --- ");
    sleep.sleep(5);

    console.log("-- RESET PINS -- ");

    gpio.setup(StepPins[0], gpio.DIR_OUT, GPIO_SetOutput_1_OFF);
    gpio.setup(StepPins[1], gpio.DIR_OUT, GPIO_SetOutput_2_OFF);
    gpio.setup(StepPins[2], gpio.DIR_OUT, GPIO_SetOutput_3_OFF);
    gpio.setup(StepPins[3], gpio.DIR_OUT, GPIO_SetOutput_4_OFF);

    console.log(" --- Pause --- ");
    sleep.sleep(4);

    gpio.setup(StepPins[0], gpio.DIR_OUT, GPIO_SetOutput_1_OFF);
    gpio.setup(StepPins[1], gpio.DIR_OUT, GPIO_SetOutput_2_OFF);
    gpio.setup(StepPins[2], gpio.DIR_OUT, GPIO_SetOutput_3_OFF);
    gpio.setup(StepPins[3], gpio.DIR_OUT, GPIO_SetOutput_4_OFF);
}

exports.launchTest = function launchTest()
{
    while(true)
    {
        //console.log("-- Beginning of the loop -- ");

        console.log(" --- StepCounter : " + StepCounter + " ---");

        for (var motorPin=0;motorPin<4;motorPin++)
        {
            if(Seq[StepCounter][motorPin]!=0)
            {
                GPIO_SetOutput(motorPin, true);
            }
            else
            {
                GPIO_SetOutput(motorPin, false);
            }
        }

        StepCounter += 1;

        // If we reach the end of the sequence
        // start again

        if (StepCounter==SeqLength)
        {
            StepCounter = 0;
        }
        if (StepCounter<0)
        {
            StepCounter = SeqLength;
        }


        /*console.log(" --- Pause --- ");
        sleep.sleep(WaitTime);
*/

        //console.log("-- End of the loop -- ");
    }
}
