#-----------------------------------
# Name: Stepper Motor
#
# Author: matt.hawkins
#
# Created: 11/07/2012
# Copyright: (c) matt.hawkins 2012
#-----------------------------------
#!/usr/bin/env python

# Import required libraries
import time
import RPi.GPIO as GPIO

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Define GPIO signals to use
# Pins 18,22,24,26
# GPIO24,GPIO25,GPIO8,GPIO7
HorizontalMotorPins = [17,18,27,22]
VerticalMotorPins = [17,18,27,22]

#HorizontalCurrentSequenceIndex = 0
#VerticalCurrentSequenceIndex = 0

HorizontalStepsPerCm = 1
VerticalStepsPerCm = 1

HorizontalStepsPerRevolution = 512
VerticalStepsPerRevolution = 512

# Define some settings
WaitTime = 0.003

# Define simple sequence
SimpleSequenceLength = 4

FW_SimpleSequence = []
FW_SimpleSequence = range(0, SimpleSequenceLength)
FW_SimpleSequence[0] = [1,0,0,0]
FW_SimpleSequence[1] = [0,1,0,0]
FW_SimpleSequence[2] = [0,0,1,0]
FW_SimpleSequence[3] = [0,0,0,1]

BW_SimpleSequence = []
BW_SimpleSequence = range(0, SimpleSequenceLength)
BW_SimpleSequence[0] = [0,0,0,1]
BW_SimpleSequence[1] = [0,0,1,0]
BW_SimpleSequence[2] = [0,1,0,0]
BW_SimpleSequence[3] = [1,0,0,0]

# Define advanced sequence
# as shown in manufacturers datasheet
AdvancedSequenceLength = 8

FW_AdvancedSequence = []
FW_AdvancedSequence = range(0, AdvancedSequenceLength)
FW_AdvancedSequence[0] = [1,0,0,0]
FW_AdvancedSequence[1] = [1,1,0,0]
FW_AdvancedSequence[2] = [0,1,0,0]
FW_AdvancedSequence[3] = [0,1,1,0]
FW_AdvancedSequence[4] = [0,0,1,0]
FW_AdvancedSequence[5] = [0,0,1,1]
FW_AdvancedSequence[6] = [0,0,0,1]
FW_AdvancedSequence[7] = [1,0,0,1]

BW_AdvancedSequence = []
BW_AdvancedSequence = range(0, AdvancedSequenceLength)
BW_AdvancedSequence[0] = [1,0,0,1]
BW_AdvancedSequence[1] = [0,0,0,1]
BW_AdvancedSequence[2] = [0,0,1,1]
BW_AdvancedSequence[3] = [0,0,1,0]
BW_AdvancedSequence[4] = [0,1,1,0]
BW_AdvancedSequence[5] = [0,1,0,0]
BW_AdvancedSequence[6] = [1,1,0,0]
BW_AdvancedSequence[7] = [1,0,0,0]



# Choose a sequence to use
ChoosenFWSequence = FW_SimpleSequence
ChoosenBWSequence = BW_SimpleSequence
ChoosenSequenceLength = SimpleSequenceLength

def resetAllMotorPins():

    HorizontalCurrentSequenceIndex = 0
    resetPins(HorizontalMotorPins)

    VerticalCurrentSequenceIndex = 0
    resetPins(VerticalMotorPins)

def resetPins(MotorPins):    # Set pins as output and false

    print "Reseting pins [%s]" %(str(MotorPins)[1:-1])

    for pin in MotorPins:
      GPIO.setup(pin,GPIO.OUT)
      GPIO.output(pin, False)



def runMotor(MotorPins, StepNumber):  # Make a motor step

    if StepNumber>0:
        ChoosenSequence = ChoosenFWSequence
    else:
        ChoosenSequence = ChoosenBWSequence

    StepCount = 0

    print "%i Steps asked" %(StepNumber)

    while StepCount<abs(StepNumber):

        SequenceCurrentIndex = 0

        while SequenceCurrentIndex<ChoosenSequenceLength:

            for pin in range(0, 4):
                xpin = MotorPins[pin]
                if ChoosenSequence[SequenceCurrentIndex][pin]!=0:
                    GPIO.output(xpin, True)
                else:
                    GPIO.output(xpin, False)

            SequenceCurrentIndex += 1

            # Wait before moving on
            time.sleep(WaitTime)

        StepCount += 1
        print " Step %i Done" %(StepCount)

def runHorizontalMotor(StepNumber):
    runMotor(HorizontalMotorPins, StepNumber)

def runVerticalMotor(StepNumber):
    runMotor(VerticalMotorPins, StepNumber)





def moveHorizontally(distance):

    NumberOfSteps = distance * HorizontalStepsPerCm

    print "Moving Horizontally of %i cm (%i steps)" %(distance, NumberOfSteps)
    runHorizontalMotor(NumberOfSteps)

def moveVertically(distance):

    NumberOfSteps = distance * VerticalStepsPerCm

    print "Moving Vertically of %i cm (%i steps)" %(distance, NumberOfSteps)
    runVerticalMotor(NumberOfSteps)



def moveUp(distance):
    moveVertically(-distance)

def moveDown(distance):
    moveVertically(distance)

def moveLeft(distance):
    moveHorizontally(-distance)

def moveRight(distance):
    moveHorizontally(distance)

def penUp():
    # Todo

def penDown():
    # Todo


